# IRAE LUCIS

### PRESENTATION

IRAE LUCIS est un jeu vidéo dans le style platformer dont le but est de passer différentes salles afin d'arriver jusqu'a la salle du boss.  
Il va falloir le battre pour terminer le jeu.  

### PARTICULARITE

Mais le jeu ne s'arrête pas la, puisque la particularité d'IRAE LUCIS est que c'est un jeu vidéo à la difficulté adaptative.  
En effet à chaque mort du joueur ou à chaque boss tué, le jeu s'adaptera à vous ! Le jeu sera plus facile si vous ne faites pas de bonnes performances,  
a contrario le jeu sera plus dur si vous êtes à l'aise.
tips : Le labyrinthe se regénère aléatoirement à chaque nouvelle partie, inutile donc d'apprendre le chemin par coeur ;)

On vous résèrve également une petite surprise au boss.

Vous avez envie de savoir ? Alors, qu'attendez-vous pour y jouer ?

### COMMENT Y JOUER ?

Vous trouverez les dossiers dans Builds en fonction de votre systeme d'exploitation, chaque dossier contient son fichier executable.

BON JEU !
